%% In this script the simulation is built and tested

%% Building the simulation with spherical hermonics

[X,Y,Z] = meshgrid(1:40);
ball = ((X-20).^2 + (Y-20).^2 + (Z-20).^2) < 200;

inside = rand(1,16);
zero = zeros(1,16);
y = randsample(16, 5);
zero(y) = 1;
inside = inside .* zero;

sim = zeros(40,40,40);

for x = 1.0:40.0
    for y = 1.0:40.0
        for z = 1.0:40.0
            if ball(x,y,z) == 1
                sim(x,y,z) = dot(inside,harmonics(x,y,z));
            else
                sim(x,y,z) = 0.5;
            end
        end
    end
end

sim = (sim - min(sim(:))) / (max(sim(:)) - min(sim(:)));

subplot(1,2,1)
montage(sim)
caxis([0,1]);
colormap gray; 
cb = colorbar;


high_noise = 0.01;
low_noise = 0.0001;
noisy_much = imnoise(sim, 'gaussian', 0, high_noise);
noisy_less = imnoise(sim, 'gaussian', 0, low_noise);
noisy_sim = zeros(40,40,40);

for x = 1.0:40.0
    for y = 1.0:40.0
        for z = 1.0:40.0
            if ball(x,y,z) == 1
                noisy_sim(x,y,z) = noisy_less(x,y,z);
            else
                noisy_sim(x,y,z) = noisy_much(x,y,z);
            end
        end
    end
end

subplot(1,2,2)
montage(noisy_sim)
caxis([0,1]);
colormap gray; 
cb = colorbar;

sim_sigma = ball * low_noise;
sim_simga = sim_sigma + ~ball * high_noise;

%% Testing TV and TV weighted on the simulation
lambda = 1;
smoothed = TV.TVL1denoise3D(noisy_sim, lambda);

lambda = 0.02;
smoothed_weighted = TV.TVL1denoise3Dweighted(noisy_sim, sim_simga, lambda);


start = 1;
stop = 40;

subplot(2,2,1);
print_horizontal(sim, start, stop);
subplot(2,2,2);
print_horizontal(noisy_sim, start, stop);
subplot(2,2,3);
print_horizontal(smoothed, start, stop);
subplot(2,2,4);
print_horizontal(smoothed_weighted, start, stop);
%% Functions

function result = harmonics(x,y,z)
    harm = zeros(1,16);
    
    harm(1) = double(1);
    harm(2) = x;
    harm(3) = z;
    harm(4) = y;   
    harm(5) = x*y;
    harm(6) = z*y;
    harm(7) = 3*z^2-(x^2+y^2+z^2);
    harm(8) = x*z;
    harm(9) = x^2 - y^2;
    harm(10) = 3*y*x^2 - y^3;
    harm(11) = x*y*z;
    harm(12) = (5*z^2 - (x^2 + y^2 + z^2))*y;
    harm(13) = 5*z^3 - 3*z*(x^2 + y^2 + z^2);
    harm(14) = (5*z^2 - (x^2 + y^2 + z^2))*x;
    harm(15) = x^2*z - y^2*z;
    harm(16) = x^3 - 3*x*y^2;    
    result = harm;
end

function print_horizontal(im, first, last)
    montage(im, 'indices', first:last);
    caxis([min(im(:)),max(im(:))])
end

function print_coronal(im, first, last)
    [height, width, depth] = size(im);
    list = zeros(width, depth, height);
    for i = 1 : height
        slice = squeeze(im(i, :, :));
        list(:,:,i) = slice;
    end
    montage(list, 'indices', first:last);
    caxis([min(list(:)),max(list(:))])
end

function print_sagittal(im, first, last)
    [height, width, depth] = size(im);
    list = zeros(height, depth, width);
    for i = 1 : height
        slice = squeeze(im(:, i, :));
        list(:,:,i) = slice;
    end
    montage(list, 'indices', first:last);
    caxis([min(list(:)),max(list(:))])
end
