%% This script tests TV and weighted TV
%% Load Data

% Mask
maskRecon = load('SampleCode & Data/subj12_run1/maskRecon.mat');
maskRecon = maskRecon.maskRecon;
maskTrust = load('SampleCode & Data/subj12_run1/maskTrust.mat');
maskTrust = maskTrust.maskTrust;
masks = maskRecon + maskTrust;

% w0_Map
w0_map = load('SampleCode & Data/subj12_run1/w0_unmasked.mat');
w0_map = w0_map.w0; 
notNeededPixel = isnan(w0_map); % Mask of where the NaNs are
w0_map(notNeededPixel) = 0; % Make nans 0

% Smoothed by the old Algorithm
w0_smoothed = load('SampleCode & Data/subj12_run1/w0_masked_smoothed.mat');
w0_smoothed = w0_smoothed.w0_smoothed; 

% SQR
sigma = load('SampleCode & Data/subj12_run1/Sigma2.mat');
sigma = sigma.Sigma2; 
notNeededPixel = isnan(sigma); % Mask of where the NaNs are
sigma(notNeededPixel) = 0; % Make nans small

%% Normalize image
w0_map = w0_map .* maskRecon;
mx = max(w0_map(:));
mn = min(w0_map(:));

norm = w0_map - mn;
norm = norm / (mx - mn); 

sigma = sigma - mn;
sigma = sigma / ((mx - mn)^2);

norm_weighted = norm;
%% Exted the recon mask by one voxel
maskEx = maskRecon;
maskPadded = padarray(maskRecon,[1 1 1],0,'both');


for y = 2:135
    for x = 2:135
        for z = 2:110
            if (maskPadded(y,x,z) == 0) && ...
                (maskPadded(y-1,x,z) == 1 || maskPadded(y,x-1,z) == 1 || maskPadded(y,x-1,z) == 1 || ...
                maskPadded(y+1,x,z) == 1 || maskPadded(y,x+1,z) == 1 || maskPadded(y,x+1,z) == 1 || ...
                maskPadded(y+1,x+1,z) == 1 || maskPadded(y,x+1,z+1) == 1 || maskPadded(y+1,x,z+1) == 1 || ...
                maskPadded(y-1,x-1,z) == 1 || maskPadded(y,x-1,z-1) == 1 || maskPadded(y-1,x,z-1) == 1)
                maskEx(y-1,x-1,z-1) = 1;
            end
        end
    end
end

maskRecon = maskEx;

%% Run TV

lambda = 1;
smoothed = TV.TVL1denoise3D(norm, lambda);
smoothed = smoothed * (mx - mn);
smoothed = smoothed + mn;
smoothed = smoothed .* maskRecon;
%% Run weighted TV

lambda_weighted = 0.0004;
smoothed_weighted = TV.TVL1denoise3Dweighted(norm_weighted, sigma, lambda_weighted);
smoothed_weighted = smoothed_weighted * (mx - mn);
smoothed_weighted = smoothed_weighted + mn;
smoothed_weighted = smoothed_weighted .* maskRecon;
%% Plot everything

% Code to print sliced pictures
start = 67;
stop = 67;

figure(1);
subplot(2,3,1)
print_horizontal(w0_map, start, stop);
title("original");
subplot(2,3,2)
print_horizontal(masks, start, stop);
title("masks");
subplot(2,3,3)
print_horizontal(sigma, start, stop);
title("Noise-variance");
subplot(2,3,4)
print_horizontal(smoothed, start, stop);
subplot(2,3,5)
print_horizontal(smoothed_weighted, start, stop);
title("smoothed with TV weighted");
subplot(2,3,6)
print_horizontal(w0_smoothed, start, stop);
title("smoothed with old algorithm");


figure(2);
subplot(2,3,1)
print_coronal(w0_map, start, stop);
title("original");
subplot(2,3,2)
print_coronal(masks, start, stop);
title("masks");
subplot(2,3,3)
print_coronal(sigma, start, stop);
title("Noise-variance");
subplot(2,3,4)
print_coronal(smoothed, start, stop);
title("smoothed with TV");
subplot(2,3,5)
print_coronal(smoothed_weighted, start, stop);
title("smoothed with TV weighted");
subplot(2,3,6)
print_coronal(w0_smoothed, start, stop);
title("smoothed with old algorithm");

figure(3);
subplot(2,3,1)
print_sagittal(w0_map, start, stop);
title("original");
subplot(2,3,2)
print_sagittal(masks, start, stop);
title("masks");
subplot(2,3,3)
print_sagittal(sigma, start, stop);
title("Noise-variance");
subplot(2,3,4)
print_sagittal(smoothed, start, stop);
subplot(2,3,5)
print_sagittal(smoothed_weighted, start, stop);
title("smoothed with TV weighted");
subplot(2,3,6)
print_sagittal(w0_smoothed, start, stop);
title("smoothed with old algorithm");

%% Helpful functions

function print_horizontal(im, first, last)
    montage(im, 'indices', first:last);
    caxis([min(im(:)),max(im(:))])
    min(im(:))
    max(im(:))
    colorbar;
end

function print_coronal(im, first, last)
    [height, width, depth] = size(im);
    list = zeros(width, depth, height);
    for i = 1 : height
        slice = squeeze(im(i, :, :));
        list(:,:,i) = slice;
    end
    montage(list, 'indices', first:last);
    caxis([min(list(:)),max(list(:))])
end

function print_sagittal(im, first, last)
    [height, width, depth] = size(im);
    list = zeros(height, depth, width);
    for i = 1 : height
        slice = squeeze(im(:, i, :));
        list(:,:,i) = slice;
    end
    montage(list, 'indices', first:last);
    cb = colorbar;
    caxis([min(list(:)),max(list(:))])
    ylabel(cb,'Off-Resonance in Hz'); 
end

