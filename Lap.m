%% Implementing and Testing Laplace inpainting

w0_map = load('SampleCode & Data/subj09_run2/w0_unmasked.mat');
w0_map = w0_map.w0; 
w0_map = w0_map/(2*pi); 
notNeededPixel = isnan(w0_map); % Mask of where the NaNs are
w0_map(notNeededPixel) = 0; % Make nans zero


mask_trust = load('SampleCode & Data/subj09_run2/maskTrust.mat');
mask_trust = mask_trust.maskTrust; 

mask_recon = load('SampleCode & Data/subj09_run2/maskRecon.mat');
mask_recon = mask_recon.maskRecon; 

w0_image = squeeze(w0_map(:,67,:));

mask_trust_im = squeeze(mask_trust(:,67,:));
mask_recon_im = squeeze(mask_recon(:,67,:));

subplot(1,3,1)
imshow(w0_image);
cb = colorbar;
ylabel(cb,'Off-Resonance in Hz'); 
caxis([-400,400])
colormap(gca,"gray");

inpaint = LaplaceInpainting(w0_image, ~mask_trust_im);
inpaint = inpaint .* mask_recon_im;
subplot(1,3,2)
imshow(inpaint);
cb = colorbar;
ylabel(cb,'Off-Resonance in Hz'); 
caxis([-400,400])
colormap(gca,"gray");

subplot(1,3,3);
imshow(mask_trust_im + mask_recon_im);
cb = colorbar;
caxis([0,2])
colormap(gca,"gray");
cb.Ticks = [0,1,2]; 
%ylabel(cb,'0: not need, 1: recon, 2: trust'); 
cb.TickLabels = {'Not Needed','Recon','Trust'};


%% 


% Image in double (not int!) and mask with 1 where you want to fill!
function newim = LaplaceInpainting(im, mask)
    u = find(mask);
    w = find(~mask);

    M = size(mask,1);
    u_north = u - 1;
    u_north = (-double(mod(u_north,size(im,1))==0)+double(mod(u_north,size(im,1))~=0)).*u_north;
    u_east = u + M;
    u_east = (-double(u_east>size(im,1)*size(im,2)) + double(u_east<=size(im,1)*size(im,2))).*u_east;
    u_south = u + 1;
    u_south = (-double(mod(u_south,size(im,1))==1)+double(mod(u_south,size(im,1))~=1)).*u_south;
    u_west = u - M;

    a = [u_north u_east u_south u_west];
    b = double(a>0);
    c = -b./(sum(b')').*b;

    v = ones(size(u));
    ijv_mask = [...
        u  u         1.00*v
        u  u_north  -0.25*v
        u  u_east   -0.25*v
        u  u_south  -0.25*v
        u  u_west   -0.25*v ];

    bad_rows = [find(ijv_mask(:,2)<1); find(ijv_mask(:,2)>size(im,1)*size(im,2))];
    ijv_mask(bad_rows,:) = [];

    ijv_nonmask = [w  w  1.00*ones(size(w))];

    ijv = [ijv_mask; ijv_nonmask];
    A = sparse(ijv(:,1),ijv(:,2),ijv(:,3));

    b = im(:);
    b(mask(:)) = 0;

    x = A\b;

    newim = reshape(x,size(im));
end


