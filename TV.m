classdef TV
    methods(Static)

%% Total variational smoothing

% TVL1denoise - TV-L1 image denoising with the primal-dual algorithm.
%
% Function denoises an image using the TV-L1 model optimized with a primal-dual
% algorithm; this works best for impulse noise. For more details, see
%   A. Mordvintsev: ROF and TV-L1 denoising with Primal-Dual algorithm,
%   http://znah.net/rof-and-tv-l1-denoising-with-primal-dual-algorithm.html
% and
%   Chambolle et al. An introduction to Total Variation for Image Analysis, 2009. <hal-00437581>
%   https://hal.archives-ouvertes.fr/hal-00437581/document
%
% Usage:
%    newim = TVL1denoise(im, lambda, niter)
%
% Arguments:
%    im          -  Image to be processed
%    lambda      -  Regularization parameter controlling the amount
%                   of denoising; smaller values imply more aggressive
%                   denoising which tends to produce more smoothed results.
%    niter       -  Number of iterations. If omitted or empty defaults
%                   to 100
%
% Returns:
%    newim       -  Denoised image with pixel values in [0 1].
% Copyright (c) 2016 Manolis Lourakis (lourakis **at** ics forth gr)
% Institute of Computer Science,
% Foundation for Research & Technology - Hellas
% Heraklion, Crete, Greece.
% http://www.ics.forth.gr/
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
%
% The Software is provided "as is", without warranty of any kind.
        function newim = TVL1denoise(im, lambda)
        niter = 100;

        L2=8.0;
        tau=0.02;
        sigma=1.0/(L2*tau);
        theta=1.0;
        lt=lambda*tau;

        [height width]=size(im); 
        unew=zeros(height, width);
        p=zeros(height, width, 2);
        d=zeros(height, width);
        ux=zeros(height, width);
        uy=zeros(height, width);
        mx=max(im(:));

        if(mx>1.0)
        nim=double(im)/double(mx); % normalize
        else
        nim=double(im); % leave intact
        end

        u=nim;
        % [p(:, :, 1), p(:, :, 2)]=imgradientxy(u, 'IntermediateDifference');

        % p(:, :, 1) = [u(2:height,:) - u(1:height-1,:) ; zeros(1,size(u,2))];
        % p(:, :, 2) = [u(:,2:width) - u(:,1:width-1), zeros(size(u,1),1)];



        p(:, :, 1)=u(:, [2:width, width]) - u;
        p(:, :, 2)=u([2:height, height], :) - u;
        for k=1:niter
        % projection
        % compute gradient in ux, uy
        [ux, uy]=imgradientxy(u, 'IntermediateDifference');
        ux=u(:, [2:width, width]) - u;
        uy=u([2:height, height], :) - u;
        p=p + sigma*cat(3, ux, uy);
        % project
        normep=max(1, sqrt(p(:, :, 1).^2 + p(:, :, 2).^2)); 
        p(:, :, 1)=p(:, :, 1)./normep;
        p(:, :, 2)=p(:, :, 2)./normep;
        % shrinkage
        % compute divergence in div
        % div = divergence(p(:,:,1), p(:,:,2));

        %     applyD1Trans = [-p(1,:,2) ; p(1:end-2,:,2) - p(2:end-1,:,2); p(end-1,:,2)];
        %     applyD2Trans = [-p(:,1,1) , p(:,1:end-2,1) - p(:,2:end-1,1), p(:,end-1,1)];
        %     
        %     div = -1 * (applyD1Trans + applyD2Trans);

        div=[p([1:height-1], :, 2); zeros(1, width)] - [zeros(1, width); p([1:height-1], :, 2)];
        div=[p(:, [1:width-1], 1)  zeros(height, 1)] - [zeros(height, 1)  p(:, [1:width-1], 1)] + div;
        %% TV-L2 model
        %unew=(u + tau*div + lt*nim)/(1+tau);
        % TV-L1 model
        v=u + tau*div;
        unew=(v-lt).*(v-nim>lt) + (v+lt).*(v-nim<-lt) + nim.*(abs(v-nim)<=lt);
        % if(v-nim>lt); unew=v-lt; elseif(v-nim<-lt) unew=v+lt; else unew=nim; end
        % extragradient step
        u=unew + theta*(unew-u);
        %% energy being minimized
        %     ux=u(:, [2:width, width]) - u;
        %     uy=u([2:height, height], :) - u;
        %     E=sum(sqrt(ux(:).^2 + uy(:).^2)) + lambda*sum(abs(u(:) - nim(:)));
        %     fprintf('Iteration %d: energy %g\n', k, E);
        end
        newim=u;
        end

%% 3D angepasst von oben

        function newim = TVL1denoise3D(im, lambda)
          niter=100; % Number of Iterations
          L2=12.0;
          tau=0.005;
          sigma=1.0/(L2*tau);
          theta=1.0;
          lt=lambda*tau;

          [height, width, depth]=size(im);

          % p stores gradients
          p=zeros(height, width, depth, 2);
          [p(:, :, :, 1), p(:, :, :, 2),  p(:, :, :, 3)]=imgradientxyz(im, 'intermediate');

          % u the picture we change
          u=im;

          for k=1:niter
            % projection
            % compute gradient in ux, uy
            [ux, uy, uz]=imgradientxyz(u, 'intermediate');
            p=p + sigma*cat(4, ux, uy, uz);

            % project
            normep=max(1, sqrt(p(:, :, :, 1).^2 + p(:, :, :, 2).^2 + p(:, :, :, 3).^2)); 
            p(:, :, :, 1)=p(:, :, :, 1)./normep;
            p(:, :, :, 2)=p(:, :, :, 2)./normep;
            p(:, :, :, 3)=p(:, :, :, 3)./normep;

            % shrinkage
            % compute divergence in div    
            div = [cat(1, p([1:height-1], :, :, 2), zeros(1, width, depth))] - [cat(1, zeros(1, width, depth),p([1:height-1], :, :, 2))];
            div = [cat(2, p(:, [1:width-1], :, 1), zeros(height, 1, depth))] - [cat(2, zeros(height, 1, depth), p(:, [1:width-1], :, 1))] + div;
            div = [cat(3, p(:, :, [1:depth-1], 3), zeros(height, width, 1))] - [cat(3, zeros(height, width, 1), p(:, :, [1:depth-1], 3))] + div;   

            % TV-L1 model
            v=u + tau*div;
            unew=(v-lt).*(v-im>lt) + (v+lt).*(v-im<-lt) + im.*(abs(v-im)<=lt);

            % extragradient step
            u=unew + theta*(unew-u);

          end
          newim=u;
        end
%% With weights
        function newim = TVL1denoise3Dweighted(im, sqr, lambda)
          niter=100; % Number of Iterations
          L2=12.0;
          tau=0.005;
          sigma=1.0/(L2*tau);
          theta=1.0;
          lt=lambda*tau;
          
          w = lt / (2*sqr);

          [height, width, depth]=size(im);
          unew = zeros(height, width, depth);

          % p stores gradients
          p=zeros(height, width, depth, 2);
          [p(:, :, :, 1), p(:, :, :, 2),  p(:, :, :, 3)]=imgradientxyz(im, 'intermediate');

          % u the picture we change
          u=im;

          for k=1:niter
            % projection
            % compute gradient in ux, uy
            [ux, uy, uz]=imgradientxyz(u, 'intermediate');
            p=p + sigma*cat(4, ux, uy, uz);

            % project
            normep=max(1, sqrt(p(:, :, :, 1).^2 + p(:, :, :, 2).^2 + p(:, :, :, 3).^2)); 
            p(:, :, :, 1)=p(:, :, :, 1)./normep;
            p(:, :, :, 2)=p(:, :, :, 2)./normep;
            p(:, :, :, 3)=p(:, :, :, 3)./normep;

            % shrinkage
            % compute divergence in div    
            div = [cat(1, p([1:height-1], :, :, 2), zeros(1, width, depth))] - [cat(1, zeros(1, width, depth),p([1:height-1], :, :, 2))];
            div = [cat(2, p(:, [1:width-1], :, 1), zeros(height, 1, depth))] - [cat(2, zeros(height, 1, depth), p(:, [1:width-1], :, 1))] + div;
            div = [cat(3, p(:, :, [1:depth-1], 3), zeros(height, width, 1))] - [cat(3, zeros(height, width, 1), p(:, :, [1:depth-1], 3))] + div;   

            % TV-L1 model
            v=u + tau*div; 
            
            for x = 1:height
                for y = 1:width
                    for z = 1:depth
                        if (v(x,y,z) - im(x,y,z) > w(x,y,z))
                            unew(x,y,z) = v(x,y,z) - w(x,y,z);
                        elseif (v(x,y,z) - im(x,y,z) < -w(x,y,z))
                            unew(x,y,z) = v(x,y,z) + w(x,y,z);
                        else
                            unew(x,y,z) = im(x,y,z);
                        end
                    end
                end
            end
            

            % extragradient step
            u=unew + theta*(unew-u);

          end
          newim=u;
        end
    end
end
